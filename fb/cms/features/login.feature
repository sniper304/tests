Feature: Login

Scenario: Login with empty login and password

  When I sign in with "" and ""
  Then the current URL should match "/admin/login"
  And I can not see CMS pages
  #And I see an alert with text "Please, enter login and password"

Scenario: Login with any login and password

  When I sign in with "qwe" and "qwe"
  Then the current URL should match "/admin/login"
  And I can not see CMS pages
  #And I see an alert with text "Please enter your email or password again"

Scenario: Login with correct login and password
  When I sign in with "dev@appfellas.co" and "dev12345"
  Then the current URL should match "/"
  And I can see all CMS pages
  Then I logout from CMS
  And the current URL should match "/admin/login"
  And I can not see CMS pages