When(/^I visit "(.*?)"$/) do |url|
  visit url
  sleep 2
end	

Then(/^I see a message with text "(.*?)"$/) do |msg|
 #page.should have_content msg  # this function works if on your site has java script popup
 expect(page).to have_content (msg)    # this function works if on your site has java script popup
end

When(/^I create a new artist "(.*?)"$/) do |usr|
  sleep 5
  click_link 'Add new'
  sleep 2 # wait 2 seconds
  fill_in 'name', :with => usr
  fill_in 'short_info', :with => usr + "test"
  fill_in 'info', :with => "full info test"+usr
  fill_in 'share_url', :with => "http://appfellas.co"
  fill_in 'spotify_playlist', :with => ""
 # attach_file('image_data', '/home/sniper304/Изображения/1.png')
 # sleep 7
 # attach_file('list_image_data', '/home/sniper304/Изображения/2.PNG')
 # sleep 7
  click_button 'Save changes'
  sleep 2 
end
      
When(/^I try to create a new user wo any data$/) do
  click_link 'Add new'
  sleep 2
  click_button 'Save changes'
  sleep 2
end

Then(/^I add name "(.*?)"$/) do |name|
  fill_in 'name', :with => name
  click_button 'Save changes'
  sleep 2
end

Then(/^I add short info "(.*?)"$/) do |s_info|
  fill_in 'short_info', :with => s_info
  click_button 'Save changes'
  sleep 2
end

Then(/^I add info "(.*?)"$/) do |info|
  fill_in 'info', :with => info
  click_button 'Save changes'
  sleep 2
end

Then(/^I add share url "(.*?)"$/) do |url|
  fill_in 'share_url', :with => url
  click_button 'Save changes'
  sleep 2
end