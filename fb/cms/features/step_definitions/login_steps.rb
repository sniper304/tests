# Authorization parameters # begin
aname="dev@appfellas.co"
apass="dev12345"
# Authorization parameters # end
hash=Hash.new
  hash={0=>"/admin/dashboard",1=>"/admin/dictionaries",2=>"/admin/excursions",3=>"/admin/infos",4=>"/admin/restaurants",5=>"/admin/settings",6=>"/admin/souvenirs",7=>"/admin/whales"}

When /^I sign in with "(.*?)" and "(.*?)"$/ do |email, pass|
  visit("/") #you must write normal link, if you don't want get error
  
  fill_in 'admin_user_email', :with => email
  fill_in 'admin_user_password', :with => pass
  click_button 'Login'
  sleep 2
end

When /^I logged user$/ do 
  visit("/") #you must write normal link, if you don't want get error
  fill_in 'admin_user_email', :with => email
  fill_in 'admin_user_password', :with => pass
  click_button 'Login'
  sleep 2
end

Then(/^the current URL should match "(.*?)"$/) do |url_pattern|
  page.driver.browser.current_url.should match(url_pattern)
end

Then(/^I see an alert with text "(.*?)"$/) do |msg|
  page.should have_content msg  # this function works if on your site has java script popup
  #expect(page).to have_content (msg)    # this function works if on your site has java script popup
end

Then(/^I logout from CMS$/) do
  visit("/admin")
  #sleep 2
  find_by_id("logout").click
end

Then(/^I can not see CMS pages$/) do
  
  8.times{|i|
    visit(hash[i])
    puts "What user try to visit:"+ hash[i]
    puts "USer should be redirected to that page: " + page.current_path
    expect(page.current_path).to eq("/admin/login")
  #sleep 1
  }
end

Then(/I can see all CMS pages$/) do
  8.times{|i|
    visit(hash[i])
    puts "What i want to visit: + #{hash[i]}"
    puts "What page i see: +#{page.current_path}"
    expect(page.current_path).to eq(hash[i])
    #sleep 2
  }
end