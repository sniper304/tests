require 'rubygems'
require 'capybara'
require 'capybara/rspec'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/poltergeist'


Capybara.run_server = false
Capybara.javascript_driver = :poltergeist
Capybara.app_host = 'http://whappy.bender.appfellas.com'

module MyCapybaraTest
  class Test
    include Capybara::DSL
    def test_whappy
      visit('/')
    end
  end
end

t = MyCapybaraTest::Test.new
t.test_whappy